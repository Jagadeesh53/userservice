const db = require('../database/db-connection');
const { multipleColumnSet,multipleColumnUpdate } = require('../utils/common.utils');

class AuthModel {
  tableName = "users";

  findByNameorEmailorMobile = async (params) => {
    const sql = `SELECT * FROM ${this.tableName} \
                    where \
                        phone_number=${params.phone_number} or \
                        email='${params.email}' or\
                        username='${params.username}'`;

    const result = await db.query(sql);
    return result;
  };
  findOne = async (params) => {
    const { columnSet, values } = multipleColumnSet(params);

    const sql = `SELECT * FROM ${this.tableName}
        WHERE ${columnSet}`;

    const result = await db.query(sql, [...values]);

    // return back the first row (user)
    return result[0];
  };
  findByName = async (params) => {
    const sql = `SELECT * FROM ${this.tableName}
        WHERE users.username Like '${params.username}%' and NOT users.id=${params.id}`;

    const result = await db.query(sql);

    return result;
  };
  findWithImage = async (params) => {
    const sql = `SELECT \
                       users.id,\
                       users.username,\
                       users.email,\
                       users.phone_number,\
                       user_image.name,\
                       user_image.pic_byte,\
                       user_image.type,\
                       users.isemailverified\
                    FROM \
                        users \
                    LEFT  JOIN     \
                        user_image \
                    ON    \
                        users.id=user_image.id \
                    where \
                         users.id NOT IN (${params.id})`;

    const result = await db.query(sql);
    return result;
  };
  findRole = async (params) => {
    //  const { columnSet, values } = multipleColumnSet(params)

    const sql = `SELECT \
                     roles.name\
                    FROM \
                        users,user_roles,roles \
                    where \
                        users.id=user_roles.user_id and \
                        users.id=${params.id} and \
                        user_roles.role_id=roles.id`;

    const result = await db.query(sql);

    // return back the first row (user)
    return result;
  };
  create = async ($data) => {
    const sql = `INSERT INTO ${this.tableName}  SET ? `;

    const result = await db.query(sql, $data);

    const id = result.insertId;

    return id;
  };
  delete = async (params) => {
    const { columnSet, values } = multipleColumnSet(params);
    const sql = `DELETE FROM ${this.tableName}
        WHERE  ${columnSet}`;

    const result = await db.query(sql, [...values]);

    const affectedRows = result ? result.affectedRows : 0;

    return affectedRows;
  };
  update = async (params, id) => {
    const { columnSet, values } = multipleColumnUpdate(params);

    const sql = `UPDATE ${this.tableName} SET ${columnSet} WHERE id = ?`;

    const result = await db.query(sql, [...values, id]);

    return result;
  };
  verifyEmail = async (token) => {

    const sql = `UPDATE ${this.tableName} SET isemailverified=1 WHERE confirmation_token = ?`;

    const result = await db.query(sql,token);

    return result;
  };
}

module.exports = new AuthModel;
