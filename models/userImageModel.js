const db = require('../database/db-connection');
const { multipleColumnSet,multipleColumnUpdate } = require('../utils/common.utils');

class UserImageModel {
    tableName = 'user_image';

  
  
    findOne = async (params) => {
        const { columnSet, values } = multipleColumnSet(params)

        const sql = `SELECT * FROM ${this.tableName}
        WHERE ${columnSet}`;
        
        const result = await db.query(sql, [...values]);

        // return back the first row (user)
        return result[0];
    }
    create = async ($data) => {
        const sql = `INSERT INTO ${this.tableName}  SET ? `
        
        const result = await db.query(sql,$data);
        const affectedRows = result ? result.affectedRows : 0;

        return  "UserImage Added" 
    }
    update = async (params, id) => {
        const { columnSet, values } = multipleColumnUpdate(params)
      
        const sql = `UPDATE ${this.tableName} SET ${columnSet} WHERE imageid = ?`;

        const result = await db.query(sql, [...values, id]);

        return result;
    }
  
}

module.exports = new UserImageModel;
