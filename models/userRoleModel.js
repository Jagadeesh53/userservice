const db = require('../database/db-connection');
const { multipleColumnSet,multipleColumnUpdate } = require('../utils/common.utils');

class UserRoleModel {
    tableName = 'user_roles';

  
  
    create = async ($data) => {
        const sql = `INSERT INTO ${this.tableName}  SET ? `
        
        const result = await db.query(sql,$data);
        const affectedRows = result ? result.affectedRows : 0;

        return  "UserRole Added" 
    }
 

  
}

module.exports = new UserRoleModel;
