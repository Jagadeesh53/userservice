
const AuthModel = require('../models/authModel');
const UserRoleModel = require('../models/userRoleModel')
const bcrypt = require('bcryptjs');
const { v4: uuidv4 } = require('uuid');
const caseConversion = require('../services/caseConversion');
const nodemail = require("../services/nodemailer");
const credentials = require("../config/config");

const appUrl = "https://linkroster.com";

var AuthController = {
  getUser: async function (req, res) {
    let user = await AuthModel.findOne({ id: req.params.userId });
    if (user) {
      var data = {};
      data.id = user.id;
      data.username = user.username;
      data.email = user.email;
      data.phoneNumber = user.phone_number;
      data.roles = await AuthModel.findRole({ id: user.id });
      data.phoneOTP = user.phoneotpcode;
      data.isFriendStatus = user.friend_status;
      data.isEmailVerified = user.isemailverified;
      data.confirmationToken = user.confirmation_token;
      data.isPhoneVerified = user.isphoneverified;

      res.status(200).json([data]);
    } else {
      res.status(401).json({ error: "User does not exist" });
    }
  },
  getUserByName: async function (req, res) {
    let array = [];
    let user = await AuthModel.findByName({
      username: req.params.userName,
      id: req.params.userId,
    });
    if (user.length > 0) {
      for (var i = 0; i < user.length; i++) {
        var data = {};
        data.id = user[i].id;
        data.username = user[i].username;
        data.email = user[i].email;
        data.phoneNumber = user[i].phone_number;
        data.roles = await AuthModel.findRole({ id: user[i].id });
        data.phoneOTP = user[i].phoneotpcode;
        data.isFriendStatus = user[i].friend_status;
        data.isEmailVerified = user[i].isemailverified;
        data.confirmationToken = user[i].confirmation_token;
        data.isPhoneVerified = user[i].isphoneverified;
        array.push(data);
      }
      res.status(200).send(array);
    } else {
      res.status(200).send(array);
    }
  },
  getUserlist: async function (req, res) {
    let user = await AuthModel.findWithImage({ id: req.params.userId });
    if (user.length > 0) {
      for (var i = 0; i < user.length; i++) {
        user[i] = caseConversion.underScoreToCamelCase(user[i]);
      }
      res.status(200).send(user);
    } else {
      res.status(401).json({ error: "User does not exist" });
    }
  },
  authenticateUser: async function (req, res) {
    const body = caseConversion.camelToUnderscore(req.body);
    const user = await AuthModel.findOne({ email: body.email });
    if (user) {
      // check user password with hashed password stored in the database
      const validPassword = await bcrypt.compare(body.password, user.password);
      if (validPassword) {
        var data = {};
        data.id = user.id;
        data.username = user.username;
        data.email = user.email;
        data.roles = await AuthModel.findRole({ id: user.id });
        if (data.roles) {
          data.roles = data.roles.map((role) => {
            return role.name;
          });
        }
        data.tokenType = "Bearer";
        data.accessToken = "11111";
        data.isEmailVerified = user.isemailverified;
        data.confirmationToken = user.confirmation_token;
        data.isPhoneVerified = user.isphoneverified;
        data.phoneNumber = user.phone_number;
        res.status(200).json(data);
      } else {
        res.status(400).json({ error: "Invalid Password" });
      }
    } else {
      res.status(401).json({ error: "User does not exist" });
    }
  },

  registerUser: async function (req, res) {
    let data = caseConversion.camelToUnderscore(req.body);

    // let dataExists = await AuthModel.findByNameorEmailorMobile({ phone_number: req.body.phoneNumber, email: req.body.email ,username:req.body.username });
    let emailExists = await AuthModel.findOne({ email: req.body.email });
    if (emailExists) {
      res.status(400).send({ message: "Error: Email is already in use!" });
    }
    let phoneNoExists = await AuthModel.findOne({
      phone_number: req.body.phoneNumber,
    });
    if (phoneNoExists) {
      res
        .status(400)
        .send({ message: "Error: PhoneNumber is already in use!" });
    }
    let usernameExists = await AuthModel.findOne({
      username: req.body.username,
    });
    if (usernameExists) {
      res.status(400).send({ message: "Error: Username is already taken!" });
    }
    if (!emailExists && !phoneNoExists && !usernameExists) {
      data.password = await bcrypt.hash(data.password, 10);
      data.isemailverified = 0;
      data.isphoneverified = 0;
      data.confirmation_token = uuidv4();
      let userId = await AuthModel.create([data]);
      var user_role = {};
      user_role.user_id = userId;
      user_role.role_id = 1;
      await UserRoleModel.create([user_role]);
      //email service
      let mailOptions = {};
      mailOptions.from = credentials.mail.gmail;
      mailOptions.to = req.body.email;
      mailOptions.subject = "Registration Confirmation";
      mailOptions.text =
        "To confirm your e-mail address, please click the link below:\n" +
        appUrl +
        "/confirm?resetToken=" +
        data.confirmation_token + 
        "\nYou may be asked to enter this confirmation code\n" +
        data.confirmation_token; 

      let result = await nodemail.Mail(mailOptions);
      console.log("1", result);
      res
        .status(200)
        .send({
          message:
            "User registered successfully!" +
            " A confirmation e-mail has been sent to" +
            req.body.mail,
        });
    }
  },
  RequestResetEmail: async function (req, res) {
    let dataExists = await AuthModel.findOne({
      email: req.params.email,
      isemailverified: 1,
    });
    console.log(dataExists);
    if (dataExists) {
      var confirmationToken = uuidv4();
      await AuthModel.update(
        { confirmation_token: confirmationToken },
        dataExists.id
      );
      //email service
      let mailOptions = {};
      mailOptions.from = credentials.mail.gmail;
      mailOptions.to = req.params.email;
      mailOptions.subject = "Password Reset Request";
      mailOptions.text =
        "To reset your password, click the link below:\n" +
        appUrl +
        "/changePassword?resetToken=" +
        confirmationToken; 

      let result = await nodemail.Mail(mailOptions);
      console.log(result);
      res
        .status(200)
        .send({ message: "A password reset link has been sent to" });
    } else {
      res.status(400).send({ message: "User Not Found with EmailId" });
    }
  },
  updatepassword: async function (req, res) {
    let dataExists = await AuthModel.findOne({
      confirmation_token: req.body.resetToken,
    });
    if (dataExists) {
      let password = await bcrypt.hash(req.body.password, 10);
      await AuthModel.update({ password: password }, dataExists.id);

      res.status(200).send({ message: "Password updated successfully!" });
    } else {
      res.status(200).send({ message: "error" });
    }
  },
  updateUser: async function (req, res) {
    let $id = Number(req.params.userId);

    if (req.body.userId) delete req.body.userId;
    let data = caseConversion.camelToUnderscore(req.body);
    const result = await AuthModel.update(data, $id);
    console.log(result.affectedRows);
    if (result.affectedRows && result.affectedRows > 0) {
      res.status(200).send({ message: "user updated successfully!" });
    } else {
      res.status(400).send("User Not Found");
    }
  },

  confirmEmail: async function (req, res) {
    let confirmation_token = req.params.token;

    const result = await AuthModel.verifyEmail(confirmation_token);

    if (result.affectedRows && result.affectedRows > 0) {
      res.status(200).send("Congratulations! Your account has been activated and email is verified!");
    } else {
      res.status(400).send(confirmation_token + "Oops!  This is an invalid confirmation link.");
    }
  }

};
module.exports = AuthController