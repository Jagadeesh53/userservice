
const AuthModel = require('../models/authModel');
const UserImageModel = require('../models/userImageModel')
const amazonS3 =require('../services/amazonS3')
const caseConversion = require('../services/caseConversion');

var UserImageController ={
  uploadimageFile: async function (req, res) {
    //console.log(req.file)
   // console.log(req.body)
    var result= await amazonS3.upload(req.file)
    console.log(result)
    if(result){
      let userImage = await UserImageModel.findOne({ id: req.params.userId });
      if(userImage){
        await UserImageModel.update({"name":req.file.originalname},userImage.imageid)
      }else{
        await UserImageModel.create({"name":req.file.originalname,"pic_byte":"","type":req.file.mimetype,"id":req.params.userId})

      }
     
      res.status(200).json({"message": `file ${req.file.originalname} uploading request submitted successfully.`});
    }else{
      res.status(400).json({"err":"something went wrong"})
    }
  },
    getUserImage: async function (req, res) {
      let userImage = await UserImageModel.findOne({ id: req.params.userId });
      if (userImage) {
        var result ={}
        result.name=userImage.name;
        result.type=userImage.type;
        result.picByte=userImage.pic_byte;
    let user = await AuthModel.findOne({ id: req.params.userId });
    if (user) {
      var data ={}
      data.id=user.id;
      data.username=user.username;
      data.email=user.email;
      data.phoneNumber=user.phone_number
      data.roles=await AuthModel.findRole({id:user.id})
      data.phoneOTP=user.phoneotpcode
      data.isFriendStatus=user.friend_status
      data.isEmailVerified=user.isemailverified;
      data.confirmationToken=user.confirmation_token;
      data.isPhoneVerified=user.isphoneverified
    } 
    result.user=data;
    res.status(200).json(result);
  }
  else {
    res.status(401).json({ error: "UserImage does not exist" });
  }
    
    },
  

};
module.exports = UserImageController