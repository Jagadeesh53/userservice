
var credentials = require("../config/config");
var AWS = require('aws-sdk'),
    fs = require('fs');

var amazonS3 = {

    upload: async function (data) {
        try {
            // For dev purposes only
            AWS.config.update({ accessKeyId: credentials.aws.accessKeyId, secretAccessKey: credentials.aws.secretAccessKey });

            var s3 = new AWS.S3();
            var result = s3.putObject({
                Bucket: credentials.aws.Bucket,
                Key: data.originalname,
                Body: data.buffer
            }, function (err, putData) {
                if (err) {
                    console.log(err)

                } else {
                    //console.log(putData)

                }
            }).promise();
            let response = await result
            //console.log(response)
            return Promise.resolve(true)
        }
        catch (err) {
            return Promise.reject(false)
        }
    }
}

module.exports = amazonS3;