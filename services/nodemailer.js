var nodemailer = require("nodemailer");
var credentials = require("../config/config");

var nodemailservice = {

    Mail: (mailOptions)=>{
        return new Promise((resolve,reject)=>{
            var transporter = nodemailer.createTransport({
            service: "gmail",
            host:"smtp.gmail.com",
            auth: {
                user: credentials.mail.gmail,
                pass: credentials.mail.password,
            },
            });

            const appUrl = "https://linkroster.com";
        
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error)
                    reject(false);
                } else {
                    console.log("Email sent: " + info.response);
                    resolve(true);
                }
            });
        })
    }
}

module.exports = nodemailservice;


// write email service
// let mailOptions = {
// };
// from: credentials.mail.gmail,
// to: data.email_id,
// subject:"",
// text:""
