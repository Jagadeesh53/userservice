var express = require('express');
var multer = require('multer');
var upload = multer();
var router = express.Router();



var AuthController = require('../controllers/authController')
var UserImageController = require('../controllers/userImageController')

router.post('/api/auth/signup', AuthController.registerUser)
router.post('/api/auth/signin', AuthController.authenticateUser)
router.get('/api/auth/requestresetEmail/:email', AuthController.RequestResetEmail)
router.post('/api/auth/updatepassword/:resetToken', AuthController.updatepassword)
router.get('/api/auth/getUsers/:userId', AuthController.getUser)
router.get('/api/auth/getUserlist/:userId', AuthController.getUserlist)
router.get('/api/auth/getUserListing/:userName/:userId', AuthController.getUserByName)
router.put('/api/auth/findalluser/:userId', AuthController.updateUser)
router.get("/api/auth/confirm/:token", AuthController.confirmEmail);

router.post('/api/test/uploadimageFile/:userId',upload.single('file'),UserImageController.uploadimageFile)

router.get('/api/test/getUserImage/:userId', UserImageController.getUserImage)

module.exports = router;